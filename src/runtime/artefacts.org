#+TITLE: Experiment Artefacts Renderer
#+AUTHOR: VLEAD
#+DATE: [2019-03-19 Tue]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document builds the artefacts renderer which, is
responsible for displaying the Lu/Tasks artefacts in the
content section of the page.

* Implementation
** Get published artefact url
Checks the type of published url.
#+NAME: get-published-art-url
#+BEGIN_SRC js
var getPublishedArtUrl = function(artfctUrl) {
  var pattern = /^(http|https)/;
  if(!pattern.test(artfctUrl)) 
    return ARTEFACTS_URL + artfctUrl;
  else
    return artfctUrl; 
};

#+END_SRC

** Filter Artefacts 
Filter the list of artefacts for a given DOM element.
#+NAME: filter-artefacts
#+BEGIN_SRC js 
var filterArtfcts = function(el) {
  var artfctsList;
  if(isTask(el) || isExplanation(el)) 
    artfctsList = getArtefacts(el);
  else
    artfctsList = getArtefacts(getPreamble(el));
  return artfctsList;
};

#+END_SRC

** Get Type of Video
Checks the type of video.
#+NAME: get-type-of-video
#+BEGIN_SRC js 
var getTypeOfVideo = function(url) {
  var youtubeRegExp = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  if(url.match(youtubeRegExp))
    return 'youtube';
  else
    return 'html5Video';
};

#+END_SRC

** Render Video Artefact
Method to render video artefact.
#+NAME: render-video-artefact
#+BEGIN_SRC js 
var renderVideoArt = function(art, url, artfctCntInnerDiv) {
  var videoType = getTypeOfVideo(url);
  if(videoType == "youtube") {
    var iframeArt = createIframe(url);
    iframeArt.setAttribute("allowFullScreen", "allowFullScreen"); 
    artfctCntInnerDiv.appendChild(createArtfct('box-vid', art, iframeArt.outerHTML));
  }
  else {
    var videoArt = createVideoSrc(getPublishedArtUrl(url)).outerHTML;
    artfctCntInnerDiv.appendChild(createArtfct('box-vid', art, videoArt));
  }
};

#+END_SRC

** Render Image Aretfact
#+NAME: render-img-artefact
#+BEGIN_SRC js
var renderImgArt = function(artfctCntInnerDiv, artfct, url, imgWidth, imgHeight) {
  var imgArt = createImgElement(getPublishedArtUrl(url), imgWidth, imgHeight);
  artfctCntInnerDiv.appendChild(createArtfct('box-img', artfct, imgArt.outerHTML));
};

#+END_SRC

** Call InteractiveJS function
Creates a div to append interactive js artefact. Here the
interactive js artefacts API in the Calls the API from the
realization catalog.
#+NAME: create-interactive-js
#+BEGIN_SRC javascript
var createInteractiveJS = function(artfctCntInnerDiv, realization){
  var func = String(realization.creator + "()" + "." + realization.api);
  var tdiv = document.createElement("div");
  tdiv.id = "showjs";
  artfctCntInnerDiv.appendChild(tdiv);
  eval(func)(tdiv);
};

#+END_SRC

** Insert Js
Injects Js scripts into a content div to render the
artefacts.
#+NAME: insert-js
#+BEGIN_SRC js
var insertJs = function(jsList, boxInnerDiv) {
  jsList.forEach(function(js) {
    var url = getPublishedArtUrl(js);
    loadScript(boxInnerDiv, url);
  });
};

#+END_SRC

** Insert Stylesheets
Injects stylesheets into a content div to render the
artefacts.
#+NAME: insert-css
#+BEGIN_SRC js
var insertCss = function(cssList, boxInnerDiv) {
  cssList.forEach(function(css) {
    var url = getPublishedArtUrl(css);
    loadCss(boxInnerDiv, url);
  });         
};

#+END_SRC

** Render Interactive Js
Renders interactive Js artefacts
#+NAME: render-interactive-js
#+BEGIN_SRC js
var renderInteractiveJs = function(boxInnerDiv, realization) {
  Promise.all([
    insertCss(realization.url["css"], boxInnerDiv),
    insertJs(realization.url["js"], boxInnerDiv)
  ]).then(function() {
    createInteractiveJS(boxInnerDiv, realization);
  }).catch(function(Error) {
    console.log(Error);
  });
};

#+END_SRC

** Create Box Layout
Creates a box layout, for displaying the artefact.
#+NAME: create-box-layout
#+BEGIN_SRC js
var createBoxLayout = function(cls) {
  var box = createHtmlElementWithClass('div', cls);
  var boxHead = createHtmlElementWithClass('h3', 'box-head');
  var boxInnerDiv = createHtmlElementWithClass('div', 'box-inner');
  box.appendChild(boxHead);
  box.appendChild(boxInnerDiv);
  return box;
};

#+END_SRC

** Create Artefact
Creates artefact in a box layout.
#+NAME: create-artefact
#+BEGIN_SRC js
var createArtfct = function(cls, artfct, artfctCnt) { 
  var box = createBoxLayout(cls);
  box.getElementsByClassName('box-head')[0].id = getShortTitle(artfct);
  box.getElementsByClassName('box-head')[0].innerHTML = getTitle(artfct);
  box.getElementsByClassName('box-inner')[0].innerHTML = artfctCnt;
  return box;
};

#+END_SRC

** Display Artefacts
Method to display artefacts based on its type.
#+NAME: display-artefacts
#+BEGIN_SRC js
var displayArtfcts = function(el) {
  var artfctCntDiv = createHtmlElementWithClass('div', 'artfct-cnt');
  var artfctCntHeadDiv = createHtmlElementWithClass('div', 'artfct-cnt-head');
  artfctCntHeadDiv.innerHTML = getTitle(el);
  var artfctCntInnerDiv = createHtmlElementWithClass('div', 'artfct-cnt-inner');
  artfctCntDiv.appendChild(artfctCntHeadDiv);
  artfctCntDiv.appendChild(artfctCntInnerDiv);

  var artfcts = filterArtfcts(el);
  artfcts.forEach(function(artfct) {
    if(isTextArtefact(artfct)) {
      let artfctCnt = getInnerHtml(getArtBody(artfct));
      artfctCntInnerDiv.appendChild(createArtfct('box-txt', artfct, artfctCnt));   
    }
    if(isImgArtefact(artfct)) {
      let artfctCnt = getArtBody(artfct);
      let img = artfctCnt.getElementsByTagName('img')[0];
      let url = img.getAttribute('src');
      let imgWidth = img.getAttribute('width');
      let imgHeight = img.getAttribute('height');
      renderImgArt(artfctCntInnerDiv, artfct, url, imgWidth, imgHeight);
    }
    if(isVideoArtefact(artfct)){
      let artfctCnt = getArtBody(artfct);
      var video = artfctCnt.getElementsByTagName('video')[0];
      var url = video.getAttribute('src');
      renderVideoArt(artfct, url, artfctCntInnerDiv);
    }
    if(isReqArtefact(artfct)){
      var realizationArt = realizationCatalog[getShortTitle(artfct)];
      if(realizationArt === undefined){
        var artfctCnt = getInnerHtml(getDescription(artfct));
        artfctCntInnerDiv.appendChild(createArtfct('box-unrealized', artfct, artfctCnt));
      }
      if(realizationArt !== undefined){
        let realizationType = realizationArt.resource_type;
        if(realizationType.toLowerCase() === 'image'){
          let url = realizationArt.url;
          let imgWidth = realizationArt.width;
          let imgHeight = realizationArt.height;
          renderImgArt(artfctCntInnerDiv, artfct, url, imgWidth, imgHeight);
        }
        if(realizationType.toLowerCase() === 'video'){
          let url = realizationArt.url;
          renderVideoArt(artfct, url, artfctCntInnerDiv);
        }
        if(realizationType.toLowerCase() === 'html'){
          let url = ARTEFACTS_URL + realizationArt.url;
          artfctCntInnerDiv.appendChild(createArtfct('box-int', artfct, createIframe(url).outerHTML));
        }
        if(realizationType.toLowerCase() === 'interactivejs'){
          let box = createBoxLayout('box-int');
          box.id = getShortTitle(artfct);
          box.getElementsByClassName('box-head')[0].innerHTML = getTitle(artfct);
          artfctCntInnerDiv.appendChild(box);
          let boxInnerDiv = box.getElementsByClassName('box-inner')[0];
          renderInteractiveJs(boxInnerDiv, realizationArt);
        }
      }
    }
  });
  return artfctCntDiv;
};

#+END_SRC

** On load Highlight Navbar Element
On load of a navbar, highlights the first navbar element.
#+NAME: onload-highlight-navbar-el
#+BEGIN_SRC js
var onLoadHighlightNavbarEl = function() {
  var artfctsUl = getArtfctsNavbarUl(); 
  var artfctLi = getObjByClassName(artfctsUl, 'artfcts-navbar-li');
  var artfctLiHref = artfctLi.getElementsByTagName('a')[0]; 
  artfctLiHref.classList.add('active-artfct');
};

#+END_SRC

** On click Highlight Navabar Element
On click of a navabar element highlights it and deactivates
the previously highlighted element.
#+NAME: onclick-highlight-navbar-el
#+BEGIN_SRC js
var onclickHighlightNavbarEl = function(clicked_el) {
  var artfctsUl = getArtfctsNavbarUl();
  var activeEl = artfctsUl.getElementsByClassName('active-artfct')[0];
  activeEl.classList.remove('active-artfct');
  clicked_el.classList.add('active-artfct');
};   

#+END_SRC

** Create Artefacts List
Creates list of artefact elements for artefacts navbar.
#+NAME: create-artefacts-list
#+BEGIN_SRC js
var createArtfctsUl =  function(artfcts) {
  var artfctsUl = createHtmlElementWithClass('ul','artfcts-navbar-ul');
  artfctsUl.classList.add('nav');
  artfctsUl.classList.add('navbar-nav');
  artfcts.forEach(function(artfct) {
    var artfctLi = createHtmlElementWithClass('li','artfcts-navbar-li');      
    var artfctLiHref = createAnchorElement(getShortTitle(artfct)); 
    artfctLi.appendChild(artfctLiHref);
    artfctLiHref.addEventListener('click', function(event){
      onclickHighlightNavbarEl(this);
    });
    artfctsUl.appendChild(artfctLi);
  });
  return artfctsUl;
};

#+END_SRC

** Create artefacts navbar
Builds artefacts navbar to display the list of artefacts.
#+NAME: create-art-navbar
#+BEGIN_SRC js
var createArtfctsNavbar = function(exp_el) {
  var artfctsNavbarDiv = createHtmlElementWithClass('div', 'artfcts-navbar');
  var navbar = createHtmlElementWithClass('nav', 'navbar');
  navbar.classList.add('navbar-default');    
  var container = createHtmlElementWithClass('div', 'container-fluid');	
  container.appendChild(createArtfctsUl(filterArtfcts(exp_el)));
  navbar.appendChild(container);
  artfctsNavbarDiv.appendChild(navbar);
  return artfctsNavbarDiv;
};

#+END_SRC

** Build Content Div
Builds content division with artefacts.
#+NAME: build-content
#+BEGIN_SRC js
var buildCnt = function(exp_el) {
  var cntDiv = getContentDiv();
  cntDiv.appendChild(createArtfctsNavbar(exp_el));
  cntDiv.appendChild(displayArtfcts(exp_el));
  cntDiv.appendChild(createPrevEl());
  cntDiv.appendChild(createNextEl());
};

#+END_SRC

** Artefacts Renderer
Main method which gets invoked to display 'Exp/Lu/Task'
artefacts in the right pane of the page.
#+NAME: artefacts-renderer
#+BEGIN_SRC js
var artfctsRenderer = function(exp_el) {  
  scrollPageToTop();  
  styleActiveTocEl(exp_el);
  clearCurrentCnt();
  buildCnt(exp_el);
  onLoadHighlightNavbarEl();
};

#+END_SRC

** API'S
These are the API's to access different divisions/elements
that are created by the artefacts renderer.
#+NAME: apis
#+BEGIN_SRC js
var getArtfctsNavBarDiv = function() {
  return getObjByClassName(getContentDiv(), 'artfcts-navbar');
};

var getArtfctsNavBar = function() {
  return getObjByClassName(getArtfctsNavBarDiv(), 'navbar');
};

var getArtfctsNavbarContainer = function() {
  return getObjByClassName(getArtfctsNavBar(), 'container-fluid');
}; 

var getArtfctsNavbarUl = function() {
  return getObjByClassName(getArtfctsNavbarContainer(), 'artfcts-navbar-ul');
}; 

var getArtfctsCntDiv = function() {
  return getObjByClassName(getContentDiv(), 'artfcts-cnt');
};
#+END_SRC

* Tangle
Tangles all the above functions and generates a file called
*artefacts.js* which has the complete implementation of the
artefacts renderer.
#+BEGIN_SRC js  :tangle js/artefacts.js :eval no :noweb yes
<<get-published-art-url>>
<<filter-artefacts>>
<<get-type-of-video>>
<<render-video-artefact>>
<<render-img-artefact>>
<<create-interactive-js>>
<<insert-js>>
<<insert-css>>
<<render-interactive-js>>
<<create-box-layout>>
<<create-artefact>>
<<display-artefacts>>
<<onload-highlight-navbar-el>>
<<onclick-highlight-navbar-el>>
<<create-artefacts-list>>
<<create-art-navbar>>
<<build-content>>
<<artefacts-renderer>>
<<apis>>
#+END_SRC
